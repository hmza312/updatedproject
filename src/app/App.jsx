import React from "react";
import { BrowserRouter, Route, Switch  } from 'react-router-dom';

import { ToastContainer } from 'react-toastify';

import IndexPage from "./pages/multiple/index_page";
import Index2Page from "./pages/multiple/index2page";
import Index3Page from "./pages/multiple/index3page";
import Index4Page from "./pages/multiple/index4page";
import Index5Page from "./pages/multiple/index5page";
import Index6Page from "./pages/multiple/index6page";
import Index7Page from "./pages/multiple/index7page";
import Index8Page from "./pages/multiple/index8page";
import Index9Page from "./pages/multiple/index9page";
import Index10Page from "./pages/multiple/index10page";
import FeaturesPage from "./pages/multiple/features_page";
import AboutPage from "./pages/multiple/about_page";
import PricingPage from "./pages/multiple/pricing_page";
import ContactPage from "./pages/multiple/contact_page";

import BlogHomePage from "./pages/blog/blog_home_page";
import BlogHomeSidebarPage from "./pages/blog/blog_home_sidebar_page";
import BlogListPage from "./pages/blog/blog_list_page";
import BlogListSidebarPage from "./pages/blog/blog_list_sidebar_page";
import BlogDetailsPage from "./pages/blog/blog_details_page";
import BlogDetailsSideBarPage from "./pages/blog/blog_details_side_bar_page";

import LandingSassPage from './pages/landing/landing_sass_page';
import LandingSassFixedImgPage from "./pages/landing/landing_sass_fixed_img_page";
import LandingProductPage from "./pages/landing/landing_product_page";
import LandingSassImagePage from "./pages/landing/landing_sass_image_page";
import LandingProductImagePage from "./pages/landing/landing_product_image_page";
import LandingSassSliderPage from "./pages/landing/landing_sass_slider_page";
import LandingProductSliderPage from "./pages/landing/landing_product_slider_page";
import LandingProductFixedImgPage from "./pages/landing/landing_product_fixed_img_page";
import LandingProductVideoPage from "./pages/landing/landing_product_video_page";
import LandingSassVideoPage from "./pages/landing/landing_sass_video_page";
import NoMatch from "./pages/NoMatch";
import ScrollToTop from "./componets/scrollToTop";
import Loader from "./componets/loader";


class App extends React.Component {

	render() {
		return (
			 <>
			<Loader/>
			<BrowserRouter basename={'/'} >
                <ScrollToTop/>
				<Switch>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index`} component={IndexPage}/>
					<Route exact path={`${process.env.PUBLIC_URL}/`} component={IndexPage}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index2`} component={Index2Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index3`} component={Index3Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index4`} component={Index4Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index5`} component={Index5Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index6`} component={Index6Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index7`} component={Index7Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index8`} component={Index8Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index9`} component={Index9Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/index10`} component={Index10Page}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/features`} component={FeaturesPage}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/about`} component={AboutPage}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/pricing`} component={PricingPage}/>
					<Route path={`${process.env.PUBLIC_URL}/multiple/contact`} component={ContactPage}/>

					<Route path={`${process.env.PUBLIC_URL}/landing/saas`} component={LandingSassPage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/sass-fixed-img`} component={LandingSassFixedImgPage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/product`} component={LandingProductPage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/sass-image`} component={LandingSassImagePage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/product-image`} component={LandingProductImagePage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/sass-slider`} component={LandingSassSliderPage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/product-slider`} component={LandingProductSliderPage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/product-fixed-img`} component={LandingProductFixedImgPage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/sass-video`} component={LandingSassVideoPage}/>
					<Route path={`${process.env.PUBLIC_URL}/landing/product-video`} component={LandingProductVideoPage}/>


					<Route path={`${process.env.PUBLIC_URL}/blog/home`} component={BlogHomePage}/>
					<Route path={`${process.env.PUBLIC_URL}/blog/home-sidebar`} component={BlogHomeSidebarPage}/>
					<Route path={`${process.env.PUBLIC_URL}/blog/list`} component={BlogListPage}/>
					<Route path={`${process.env.PUBLIC_URL}/blog/list-sidebar`} component={BlogListSidebarPage}/>
					<Route path={`${process.env.PUBLIC_URL}/blog/details`} component={BlogDetailsPage}/>
					<Route path={`${process.env.PUBLIC_URL}/blog/details-sidebar`} component={BlogDetailsSideBarPage}/>

					<Route component={NoMatch} />
				</Switch>
				<ToastContainer />
			</BrowserRouter>

			 </>
		 );
	}
}
 
export default App;
